package amqpclient

import (
	"os"
	"strconv"
)

//CreateEvent -
func (pvec ApprovalEventComposer) CreateEvent(event ApprovalEvent) AMQPMessage {
	message := AMQPMessage{}
	message.Event = Event.Approval.CreateEvent
	postMap := make(map[string]interface{})

	postMap["id"] = strconv.FormatInt(event.ID, 10)
	postMap["user_id"] = strconv.FormatInt(event.UserID, 10)
	postMap["requested_by"] = strconv.FormatInt(event.RequestedBy, 10)
	postMap["resource_id"] = strconv.FormatInt(event.ResourceID, 10)
	postMap["resource_type"] = event.ResourceType
	postMap["comment"] = event.Comment
	postMap["status"] = event.Status
	postMap["created_at"] = event.CreatedAt

	message.Sender = os.Getenv("app.name")
	message.Content = postMap
	return message
}
