package amqpclient

import "strconv"

//WalletTransactionPerUserProcessor -
type WalletTransactionPerUserProcessor struct {
}

//CreateEvent -
func (wrpup WalletTransactionPerUserProcessor) CreateEvent(amqpMessage AMQPMessage) WalletTransactionPerUserCreateModel {

	postMap := amqpMessage.Content.(map[string]interface{})
	transaction := WalletTransactionPerUserCreateModel{}
	transaction.UserID, _ = strconv.ParseInt(postMap["user_id"].(string), 10, 64)
	transactionMap := postMap["transactions"].(map[string]interface{})

	transactions := []WalletTransactionCreateModel{}

	for key, value := range transactionMap {
		wt := WalletTransactionCreateModel{}
		amt, _ := strconv.ParseFloat(value.(string), 32)
		wt.Amount = float32(amt)
		wt.FromSourceID, _ = strconv.ParseInt(key, 10, 64)
		transactions = append(transactions, wt)
	}
	transaction.Transactions = transactions
	return transaction

}
