package amqpclient

import (
	"fmt"
	"os"
	"strconv"
)

//WalletTransactionComposer -
type WalletTransactionComposer struct {
}

//CreateEvent -
func (wtc WalletTransactionComposer) CreateEvent(createModel WalletTransactionCreateModel) AMQPMessage {
	message := AMQPMessage{}
	message.Event = Event.WalletTransaction.CreateEvent
	wtMap := make(map[string]interface{})
	wtMap["id"] = strconv.FormatInt(createModel.ID, 10)
	wtMap["comment"] = createModel.Comment
	wtMap["external_transaction_id"] = createModel.ExternalTransactionID
	wtMap["from_source_id"] = strconv.FormatInt(createModel.FromSourceID, 10)
	wtMap["status"] = strconv.Itoa(createModel.Status)
	wtMap["transaction_type"] = strconv.Itoa(createModel.TransactionType)
	wtMap["user_id"] = strconv.FormatInt(createModel.UserID, 10)
	wtMap["wallet_id"] = strconv.FormatInt(createModel.WalletID, 10)
	wtMap["amount"] = fmt.Sprintf("%f", createModel.Amount)
	message.Sender = os.Getenv("app.name")
	message.Content = wtMap
	return message
}

//UpdateEvent -
func (wtc WalletTransactionComposer) UpdateEvent(createModel WalletTransactionUpdateModel) AMQPMessage {
	message := AMQPMessage{}
	message.Event = Event.WalletTransaction.CreateEvent
	wtMap := make(map[string]interface{})
	wtMap["id"] = strconv.FormatInt(createModel.ID, 10)
	wtMap["status"] = strconv.Itoa(createModel.Status)
	message.Sender = os.Getenv("app.name")
	message.Content = wtMap
	return message
}
