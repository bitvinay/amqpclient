package amqpclient

import (
	"os"
	"strconv"
)

//CreateEvent -
func (pvec PostReportEventComposer) CreateEvent(event PostReportEvent) AMQPMessage {
	message := AMQPMessage{}
	message.Event = Event.Post.Like.CreateEvent
	postMap := make(map[string]interface{})

	postMap["id"] = strconv.FormatInt(event.ID, 10)
	postMap["user_id"] = strconv.FormatInt(event.UserID, 10)
	postMap["post_id"] = strconv.FormatInt(event.PostID, 10)
	postMap["post_id"] = strconv.FormatInt(event.PostID, 10)
	postMap["report_type"] = event.ReportType
	postMap["report_description"] = event.ReportDescription

	message.Sender = os.Getenv("app.name")
	message.Content = postMap
	return message
}
