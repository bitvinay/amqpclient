package counter

import (
	"os"
	"strconv"

	"gitlab.com/bitvinay/amqpclient"
)

//CounterEventProcessor -
type CounterEventProcessor struct {
}

//CreateEvent - create counter event
func (uec CounterEventProcessor) CreateEvent(amqpMessage amqpclient.AMQPMessage) CounterModel {

	counterMap := amqpMessage.Content.(map[string]interface{})
	counterModel := CounterModel{}

	counterModel.Type = counterMap["type"].(string)
	counterModel.Data = counterMap["data"].(string)

	return counterModel
}

//DeleteEvent -  delete event
func (uec CounterEventProcessor) DeleteEvent(counterType string, id int64) amqpclient.AMQPMessage {
	message := amqpclient.AMQPMessage{}
	message.Event = amqpclient.Event.Counter.DeleteEvent
	tagMap := make(map[string]interface{})
	tagMap["type"] = counterType
	tagMap["data"] = strconv.FormatInt(id, 10)
	message.Sender = os.Getenv("app.name")
	message.Content = tagMap
	return message
}
