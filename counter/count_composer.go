package counter

import (
	"os"

	"gitlab.com/bitvinay/amqpclient"
)

//CounterModel -
type CounterModel struct {
	Type string `json:"type"`
	Data string `json:"data"`
}

//CounterEventComposer -
type CounterEventComposer struct {
}

//CreateEvent - create counter event
func (uec CounterEventComposer) CreateEvent(counterModel CounterModel) amqpclient.AMQPMessage {
	message := amqpclient.AMQPMessage{}
	message.Event = amqpclient.Event.Counter.UpdateEvent
	tagMap := make(map[string]interface{})
	tagMap["type"] = counterModel.Type
	tagMap["data"] = counterModel.Data
	message.Sender = os.Getenv("app.name")
	message.Content = tagMap
	return message
}

//DeleteEvent -  delete event
func (uec CounterEventComposer) DeleteEvent(counterModel CounterModel) amqpclient.AMQPMessage {
	message := amqpclient.AMQPMessage{}
	message.Event = amqpclient.Event.Counter.DeleteEvent
	tagMap := make(map[string]interface{})
	tagMap["type"] = counterModel.Type
	tagMap["data"] = counterModel.Data
	message.Sender = os.Getenv("app.name")
	message.Content = tagMap
	return message
}
