package amqpclient

import (
	"os"
	"strconv"
)

//CreateEvent - create post action event
func (uec PostActionEventComposer) CreateEvent(post PostAddModel) AMQPMessage {
	message := AMQPMessage{}
	message.Event = Event.Post.Action.CreateEvent
	userMap := make(map[string]interface{})
	userMap["id"] = strconv.FormatInt(post.ID, 10)
	userMap["channel_id"] = strconv.FormatInt(post.ChannelID, 10)
	userMap["text"] = post.Text
	userMap["created_at"] = post.CreatedAt
	message.Sender = os.Getenv("app.name")
	message.Content = userMap
	return message
}

//DeleteEvent -
func (uec PostActionEventComposer) DeleteEvent(post PostDeleteModel) AMQPMessage {
	message := AMQPMessage{}
	message.Event = Event.Post.Action.DeleteEvent
	message.Sender = os.Getenv("app.name")
	message.Content = strconv.FormatInt(post.ID, 10)
	return message
}
