package amqpclient

//EventType -
type EventType string

type event struct {
	Channel           channel
	User              user
	Tag               tag
	Comment           comment
	SubscribedChannel subscribedChannel
	Post              post
	WalletTransaction walletTransaction
	Counter           counterEvent
	Approval          approvalEvent
}

type approvalEvent struct {
	CreateEvent EventType
	UpdateEvent EventType
}
type counterEvent struct {
	UpdateEvent EventType
	DeleteEvent EventType
}

type subscribedChannel struct {
	CreateEvent EventType
	UpdateEvent EventType
	DeleteEvent EventType
}
type comment struct {
	CreateEvent EventType
	UpdateEvent EventType
	DeleteEvent EventType
}
type count struct {
	IncrementEvent EventType
	DecrementEvent EventType
}
type channelMetadata struct {
	PostCount     count
	FollowerCount count
}

type channel struct {
	CreateEvent EventType
	UpdateEvent EventType
	DeleteEvent EventType
	Metadata    channelMetadata
}

type user struct {
	CreateEvent EventType
	UpdateEvent EventType
	DeleteEvent EventType
}
type tag struct {
	CreateEvent EventType
	Metadata    tagMetadata
}
type tagMetadata struct {
	FolloweCount    count
	PostCount       count
	PopularityIndex count
}
type post struct {
	CreateEvent     EventType
	DeleteEvent     EventType
	PostTag         postTag
	PostContent     postContent
	Metadata        postMetadata
	Action          postAction
	View            EventType
	ViewTransaction EventType
	Like            postLike
	Report          postReport
}
type postReport struct {
	CreateEvent EventType
}
type postLike struct {
	CreateEvent EventType
	DeleteEvent EventType
}
type postMetadata struct {
	ViewCount    count
	ShareCount   count
	CommentCount count
}

type postTag struct {
	CreateEvent EventType
}
type postContent struct {
	CreateEvent EventType
}

type postAction struct {
	CreateEvent EventType
	DeleteEvent EventType
}
type walletTransaction struct {
	CreateEvent        EventType
	UpdateEvent        EventType
	PerUserCreateEvent EventType
}

//Event -
var Event = event{
	Channel: channel{
		CreateEvent: "channel.create",
		UpdateEvent: "channel.update",
		DeleteEvent: "channel.delete",
		Metadata: channelMetadata{
			PostCount: count{
				IncrementEvent: "channel.metadata.post.increment",
				DecrementEvent: "channel.metadata.post.decrement",
			},
			FollowerCount: count{
				IncrementEvent: "channel.metadata.follower.increment",
				DecrementEvent: "channel.metadata.follower.decrement",
			},
		},
	},
	User: user{
		CreateEvent: "user.create",
		UpdateEvent: "user.update",
		DeleteEvent: "user.delete",
	},
	Tag: tag{
		CreateEvent: "tag.create",
	},
	Comment: comment{
		CreateEvent: "comment.create",
		UpdateEvent: "comment.update",
		DeleteEvent: "comment.delete",
	},
	SubscribedChannel: subscribedChannel{
		CreateEvent: "subscribe.channel.create",
		UpdateEvent: "subscribe.channel.update",
		DeleteEvent: "subscribe.channel.delete",
	},
	Post: post{
		CreateEvent: "post.create",
		DeleteEvent: "post.delete",
		PostTag: postTag{
			CreateEvent: "post.tag.create",
		},
		PostContent: postContent{
			CreateEvent: "post.content.create",
		},
		Action: postAction{
			CreateEvent: "post.action.create",
			DeleteEvent: "post.action.delete",
		},
		Metadata: postMetadata{
			ViewCount: count{
				IncrementEvent: "post.metadata.view.increment",
				DecrementEvent: "post.metadata.view.decrement",
			},
			ShareCount: count{
				IncrementEvent: "post.metadata.share.increment",
				DecrementEvent: "post.metadata.share.decrement",
			},
			CommentCount: count{
				IncrementEvent: "post.metadata.comment.increment",
				DecrementEvent: "post.metadata.comment.decrement",
			},
		},
		View: "post.view.event",
		Like: postLike{
			CreateEvent: "post.like.create",
			DeleteEvent: "post.like.delete",
		},
		Report: postReport{
			CreateEvent: "post.report.create",
		},
	},
	WalletTransaction: walletTransaction{
		CreateEvent:        "wallet.transaction.create",
		UpdateEvent:        "wallet.transaction.update",
		PerUserCreateEvent: "wallet.transaction.per.user.create",
	},
	Counter: counterEvent{
		UpdateEvent: "counter.generic.update",
		DeleteEvent: "counter.generic.delete",
	},
	Approval: approvalEvent{
		CreateEvent: "approval.request.create",
		UpdateEvent: "approval.request.update",
	},
}
