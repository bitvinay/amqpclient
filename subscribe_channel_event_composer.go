package amqpclient

import (
	"os"
	"strconv"
)

//CreateEvent -Subscribe Channel event
func (cec SubscribeChannelEventComposer) CreateEvent(subscribeChannel SubscribeChannelAddModel) AMQPMessage {
	message := AMQPMessage{}
	message.Event = Event.SubscribedChannel.CreateEvent
	subscribeMap := make(map[string]interface{})
	subscribeMap["id"] = strconv.FormatInt(subscribeChannel.ID, 10)
	subscribeMap["channel_id"] = strconv.FormatInt(subscribeChannel.ChannelID, 10)
	subscribeMap["user_id"] = strconv.FormatInt(subscribeChannel.UserID, 10)
	subscribeMap["active"] = subscribeChannel.Active
	subscribeMap["created_at"] = subscribeChannel.CreatedAt
	message.Content = subscribeMap
	message.Sender = os.Getenv("app.name")
	return message
}

//UpdateEvent -Subscribe update Channel event
func (cec SubscribeChannelEventComposer) UpdateEvent(subscribeChannel SubscribeChannelUpdateModel) AMQPMessage {
	message := AMQPMessage{}
	message.Event = Event.SubscribedChannel.UpdateEvent
	subscribeMap := make(map[string]interface{})
	subscribeMap["id"] = strconv.FormatInt(subscribeChannel.ID, 10)
	subscribeMap["active"] = subscribeChannel.Active
	subscribeMap["updated_at"] = subscribeChannel.UpdatedAt
	message.Content = subscribeMap
	message.Sender = os.Getenv("app.name")
	return message
}

//DeleteEvent -Subscribe Channel event
func (cec SubscribeChannelEventComposer) DeleteEvent(subscribeChannel SubscribeChannelDeleteModel) AMQPMessage {
	message := AMQPMessage{}
	message.Event = Event.SubscribedChannel.DeleteEvent
	message.Sender = os.Getenv("app.name")
	subscribeMap := make(map[string]interface{})
	subscribeMap["channel_id"] = strconv.FormatInt(subscribeChannel.ChannelID, 10)
	subscribeMap["user_id"] = strconv.FormatInt(subscribeChannel.UserID, 10)
	message.Content = subscribeMap
	return message
}
