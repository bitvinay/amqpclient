package amqpclient

import (
	"os"
	"strconv"
)

//WalletTransactionPerUserComposer -
type WalletTransactionPerUserComposer struct {
}

//CreateEvent -
func (pvec WalletTransactionPerUserComposer) CreateEvent(event WalletTransactionPerUserCreateModel) AMQPMessage {
	message := AMQPMessage{}
	message.Event = Event.WalletTransaction.PerUserCreateEvent
	postMap := make(map[string]interface{})
	channelTransaction := make(map[string]string)

	for _, transaction := range event.Transactions {
		key := strconv.FormatInt(transaction.FromSourceID, 10)
		value := strconv.FormatFloat(float64(transaction.Amount), 'E', 2, 32)
		channelTransaction[key] = value
	}
	postMap["transactions"] = channelTransaction
	postMap["user_id"] = strconv.FormatInt(event.UserID, 10)
	message.Sender = os.Getenv("app.name")
	message.Content = postMap
	return message
}
