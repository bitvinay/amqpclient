package amqpclient

import (
	"strconv"
	"time"
)

//CreateEvent -
func (uep ChannelEventProcessor) CreateEvent(amqpMessage AMQPMessage) ChannelAddModel {
	channelMap := amqpMessage.Content.(map[string]interface{})
	channel := ChannelAddModel{}
	channel.ID, _ = strconv.ParseInt(channelMap["id"].(string), 10, 64)

	if channelMap["description"] != nil {
		channel.Description = channelMap["description"].(string)
	}
	if channelMap["name"] != nil {
		channel.Name = channelMap["name"].(string)
	}
	if channelMap["unique_name"] != nil {
		channel.UniqueName = channelMap["unique_name"].(string)
	}
	if channelMap["user_id"] != nil {
		channel.UserID, _ = strconv.ParseInt(channelMap["user_id"].(string), 10, 64)
	}
	if channelMap["type"] != nil {
		channel.Type = channelMap["type"].(bool)
	}
	if channelMap["created_at"] != nil {
		channel.CreatedAt, _ = time.Parse(time.RFC3339, channelMap["created_at"].(string))
	}
	return channel
}

//UpdateEvent -
func (uep ChannelEventProcessor) UpdateEvent(amqpMessage AMQPMessage) ChannelUpdateModel {
	channelMap := amqpMessage.Content.(map[string]interface{})
	channel := ChannelUpdateModel{}
	channel.ID, _ = strconv.ParseInt(channelMap["id"].(string), 10, 64)

	if channelMap["description"] != nil {
		channel.Description = channelMap["description"].(string)
	}
	if channelMap["name"] != nil {
		channel.Name = channelMap["name"].(string)
	}
	if channelMap["pic_url"] != nil {
		channel.PicURL = channelMap["pic_url"].(string)
	}
	if channelMap["thumb_url"] != nil {
		channel.ThumbURL = channelMap["thumb_url"].(string)
	}
	if channelMap["type"] != nil {
		channel.Type = channelMap["type"].(bool)
	}
	return channel
}

//DeleteEvent -
func (uep ChannelEventProcessor) DeleteEvent(amqpMessage AMQPMessage) ChannelDeleteModel {
	channel := ChannelDeleteModel{}
	channel.ID, _ = strconv.ParseInt(amqpMessage.Content.(string), 10, 64)
	return channel
}
