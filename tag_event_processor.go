package amqpclient

import "strconv"

//CreateEvent -
func (uep TagEventProcessor) CreateEvent(amqpMessage AMQPMessage) TagAddModel {
	tagMap := amqpMessage.Content.(map[string]interface{})
	tag := TagAddModel{}
	tag.ID, _ = strconv.ParseInt(tagMap["id"].(string), 10, 64)
	tag.Name = tagMap["name"].(string)
	tag.CreatedBy, _ = strconv.ParseInt(tagMap["created_by"].(string), 10, 64)
	return tag
}
