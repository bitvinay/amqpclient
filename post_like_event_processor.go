package amqpclient

import "strconv"

//CreateEvent -
func (pevp PostLikeEventProcessor) CreateEvent(amqpMessage AMQPMessage) PostLikeEvent {

	postMap := amqpMessage.Content.(map[string]interface{})
	postViewEvent := PostLikeEvent{}
	postViewEvent.ID, _ = strconv.ParseInt(postMap["id"].(string), 10, 64)
	postViewEvent.UserID, _ = strconv.ParseInt(postMap["user_id"].(string), 10, 64)
	postViewEvent.PostID, _ = strconv.ParseInt(postMap["post_id"].(string), 10, 64)
	return postViewEvent

}

//DeleteEvent -
func (pevp PostLikeEventProcessor) DeleteEvent(amqpMessage AMQPMessage) PostLikeEvent {

	postMap := amqpMessage.Content.(map[string]interface{})
	postViewEvent := PostLikeEvent{}

	postViewEvent.UserID, _ = strconv.ParseInt(postMap["user_id"].(string), 10, 64)
	postViewEvent.PostID, _ = strconv.ParseInt(postMap["post_id"].(string), 10, 64)
	return postViewEvent

}
