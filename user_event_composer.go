package amqpclient

import (
	"os"
	"strconv"
)

//CreateEvent - create user event
func (uec UserEventComposer) CreateEvent(user UserAddModel) AMQPMessage {
	message := AMQPMessage{}
	message.Event = Event.User.CreateEvent
	userMap := make(map[string]interface{})
	userMap["id"] = strconv.FormatInt(user.ID, 10)
	userMap["name"] = user.Name
	userMap["email"] = user.Email
	userMap["country"] = user.Country
	userMap["phone"] = user.Phone
	userMap["active"] = user.Active
	userMap["user_name"] = user.UserName
	message.Sender = os.Getenv("app.name")
	message.Content = userMap
	return message
}

//UpdateEvent - update user event
func (uec UserEventComposer) UpdateEvent(user UserUpdateModel) AMQPMessage {
	message := AMQPMessage{}
	message.Event = Event.User.UpdateEvent
	userMap := make(map[string]interface{})
	userMap["id"] = strconv.FormatInt(user.ID, 10)
	userMap["name"] = user.Name
	userMap["email"] = user.Email
	userMap["country"] = user.Country
	userMap["phone"] = user.Phone
	userMap["active"] = user.Active
	userMap["user_name"] = user.UserName
	userMap["gender"] = user.Gender
	userMap["profile_pic_url"] = user.ProfilePicURL
	userMap["thumb_url"] = user.ThumbPicURL
	userMap["dob"] = user.DOB
	message.Sender = os.Getenv("app.name")
	message.Content = userMap
	return message
}

//DeleteEvent -
func (uec UserEventComposer) DeleteEvent(userID int64) AMQPMessage {
	message := AMQPMessage{}
	message.Event = Event.User.DeleteEvent
	message.Sender = os.Getenv("app.name")
	message.Content = strconv.FormatInt(userID, 10)
	return message
}
