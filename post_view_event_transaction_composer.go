package amqpclient

import (
	"os"
	"strconv"
)

//PostViewEventTransactionComposer -
type PostViewEventTransactionComposer struct {
}

//CreateEvent -
func (pvec PostViewEventTransactionComposer) CreateEvent(event PostViewEventTransaction) AMQPMessage {
	message := AMQPMessage{}
	message.Event = Event.Post.ViewTransaction
	postMap := make(map[string]interface{})

	postMap["post_id"] = strconv.FormatInt(event.PostID, 10)
	postMap["channel_id"] = strconv.FormatInt(event.ChannelID, 10)
	postMap["user_id"] = strconv.FormatInt(event.UserID, 10)
	postMap["advt_id"] = strconv.FormatInt(event.AdvertisementID, 10)
	postMap["owner_user_id"] = strconv.FormatInt(event.OwnerUserID, 10)

	message.Sender = os.Getenv("app.name")
	message.Content = postMap
	return message
}
