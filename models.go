package amqpclient

import "time"

//AMQPMessage object
type AMQPMessage struct {
	Event      EventType   `json:"event"`
	Content    interface{} `json:"content"`
	Sender     string      `json:"sender"`
	ToExchange string      `json:"toExchange"`
}

//TagAddModel -
type TagAddModel struct {
	ID        int64  `json:"id"`
	Name      string `json:"name"`
	CreatedBy int64  `json:"createdBy"`
}

//PostAddModel -
type PostAddModel struct {
	ID          int64     `json:"id"`
	Text        string    `json:"text"`
	ChannelID   int64     `json:"channelId"`
	UserID      int64     `json:"userId"`
	Attachments string    `json:"attachments"`
	Tags        string    `json:"tags"`
	CreatedAt   time.Time `json:"createdAt"`
}

//PostDeleteModel -
type PostDeleteModel struct {
	ID int64 `json:"id"`
}

//ChannelAddModel -
type ChannelAddModel struct {
	ID          int64     `json:"id"`
	Name        string    `json:"name"`
	UniqueName  string    `json:"uniqueName"`
	Description string    `json:"description"`
	UserID      int64     `json:"userId"`
	Type        bool      `json:"type"`
	CreatedAt   time.Time `json:"createdAt"`
}

//ChannelUpdateModel -
type ChannelUpdateModel struct {
	ID          int64  `json:"id"`
	Name        string `json:"name"`
	Description string `json:"description"`
	Type        bool   `json:"type"`
	PicURL      string `json:"picURL"`
	ThumbURL    string `json:"thumbURL"`
}

//ChannelDeleteModel -
type ChannelDeleteModel struct {
	ID int64 `json:"id"`
}

// UserAddModel -
type UserAddModel struct {
	ID       int64  `json:"id"`
	Name     string `json:"name"`
	UserName string `json:"userName"`
	Email    string `json:"email"`
	Country  string `json:"country"`
	Phone    string `json:"phone"`
	Active   bool   `json:"active"`
}

//UserUpdateModel - update
type UserUpdateModel struct {
	ID            int64  `json:"id"`
	Name          string `json:"name"`
	UserName      string `json:"userName"`
	Email         string `json:"email"`
	Country       string `json:"country"`
	Phone         string `json:"phone"`
	Active        bool   `json:"active"`
	Gender        string `json:"gender"`
	DOB           string `json:"dob"`
	ProfilePicURL string `json:"profilePicURL"`
	ThumbPicURL   string `json:"thumbPicURL"`
}

//UserDeleteModel - update
type UserDeleteModel struct {
	ID int64 `json:"id"`
}

//SubscribeChannelAddModel -
type SubscribeChannelAddModel struct {
	ID        int64     `json:"id"`
	ChannelID int64     `json:"channelId"`
	UserID    int64     `json:"userId"`
	Active    bool      `json:"active"`
	CreatedAt time.Time `json:"createdAt"`
}

//SubscribeChannelUpdateModel -
type SubscribeChannelUpdateModel struct {
	ID        int64     `json:"id"`
	Active    bool      `json:"active"`
	UpdatedAt time.Time `json:"createdAt"`
}

//SubscribeChannelDeleteModel -
type SubscribeChannelDeleteModel struct {
	ChannelID int64 `json:"channelId"`
	UserID    int64 `json:"userId"`
}

//PostActionAddModel -
type PostActionAddModel struct {
	ID        int64     `json:"postActionId"`
	PostID    int64     `json:"postId"`
	Action    int       `json:"action"`
	ActionBy  int64     `json:"actionBy"`
	CreatedAt time.Time `json:"createdAt"`
}

//PostActionDeleteModel -
type PostActionDeleteModel struct {
	PostID   int64 `json:"postId"`
	Action   int64 `json:"action"`
	ActionBy int64 `json:"actionBy"`
}

// PostMetadataUpdateModel -
type PostMetadataUpdateModel struct {
	PostID    int64 `json:"postId"`
	Increment bool  `json:"increment"`
}

// CommentAddModel -
type CommentAddModel struct {
	ID        int64     `json:"id"`
	Text      string    `json:"text"`
	PostID    int64     `json:"postId"`
	UserID    int64     `json:"userId"`
	ParentID  int64     `json:"parentId"`
	CreatedAt time.Time `json:"createdAt"`
}

//CommentUpdateModel -
type CommentUpdateModel struct {
	ID        int64     `json:"id"`
	Text      string    `json:"text"`
	UpdatedAt time.Time `json:"updatedAt"`
}

//CommentDeleteModel -
type CommentDeleteModel struct {
	ID     int64 `json:"id"`
	PostID int64 `json:"postId"`
	UserID int64 `json:"userId"`
}

//PostViewEvent -
type PostViewEvent struct {
	PostUser        map[string]int64 `json:"postUser"`
	UserID          int64            `json:"userId"`
	AdvertisementID int64            `json:"advertisementId"`
}

//PostViewEventTransaction -
type PostViewEventTransaction struct {
	PostID          int64 `json:"postId"`
	UserID          int64 `json:"userId"`
	ChannelID       int64 `json:"channelId"`
	OwnerUserID     int64 `json:"ownerUserId"`
	AdvertisementID int64 `json:"advertisementId"`
}

//WalletTransactionPerUserCreateModel -
type WalletTransactionPerUserCreateModel struct {
	UserID       int64                          `json:"userId"`
	Transactions []WalletTransactionCreateModel `json:"transactions"`
}

// WalletTransactionCreateModel -
type WalletTransactionCreateModel struct {
	ID                    int64   `json:"id"`
	TransactionType       int     `json:"transactionType"`
	ExternalTransactionID string  `json:"externalTransactionId"`
	Status                int     `json:"status"`
	Comment               string  `json:"comment"`
	WalletID              int64   `json:"walletId"`
	UserID                int64   `json:"userId"`
	Amount                float32 `json:"amount"`
	FromSourceID          int64   `json:"fromSourceId"`
}

//WalletTransactionUpdateModel -
type WalletTransactionUpdateModel struct {
	ID     int64 `json:"id"`
	Status int   `json:"status"`
}

//PostLikeEvent -
type PostLikeEvent struct {
	ID     int64 `json:"id"`
	PostID int64 `json:"postId"`
	UserID int64 `json:"userId"`
}

//PostReportEvent -
type PostReportEvent struct {
	ID                int64  `json:"id"`
	PostID            int64  `json:"postId"`
	UserID            int64  `json:"userId"`
	ReportType        string `json:"reportType"`
	ReportDescription string `json:"reportDescription"`
}

//ApprovalEvent -
type ApprovalEvent struct {
	ID           int64     `json:"id"`
	ResourceID   int64     `json:"resourceId"`
	ResourceType string    `json:"resourceType"`
	Comment      string    `json:"comment"`
	RequestedBy  int64     `json:"requestedBy"`
	UserID       int64     `json:"userId"`
	Status       bool      `json:"status"`
	CreatedAt    time.Time `json:"createdAt"`
}
