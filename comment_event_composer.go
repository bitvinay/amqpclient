package amqpclient

import (
	"os"
	"strconv"
)

//CreateEvent -Channel event
func (cec CommentEventComposer) CreateEvent(comment CommentAddModel) AMQPMessage {
	message := AMQPMessage{}
	message.Event = Event.Comment.CreateEvent
	commentMap := make(map[string]interface{})
	commentMap["id"] = strconv.FormatInt(comment.ID, 10)
	commentMap["text"] = comment.Text
	commentMap["post_id"] = strconv.FormatInt(comment.PostID, 10)
	commentMap["user_id"] = strconv.FormatInt(comment.UserID, 10)
	commentMap["parent_id"] = strconv.FormatInt(comment.ParentID, 10)
	commentMap["created_at"] = comment.CreatedAt
	message.Content = commentMap
	message.Sender = os.Getenv("app.name")
	return message
}

//UpdateEvent -Channel event
func (cec CommentEventComposer) UpdateEvent(comment CommentUpdateModel) AMQPMessage {
	message := AMQPMessage{}
	message.Event = Event.Comment.UpdateEvent
	commentMap := make(map[string]interface{})
	commentMap["id"] = strconv.FormatInt(comment.ID, 10)
	commentMap["text"] = comment.Text
	commentMap["updated_at"] = comment.UpdatedAt
	message.Content = commentMap
	message.Sender = os.Getenv("app.name")
	return message
}

//DeleteEvent -Channel event
func (cec CommentEventComposer) DeleteEvent(comment CommentDeleteModel) AMQPMessage {
	message := AMQPMessage{}
	message.Event = Event.Comment.DeleteEvent
	message.Sender = os.Getenv("app.name")
	commentMap := make(map[string]interface{})
	commentMap["id"] = strconv.FormatInt(comment.ID, 10)
	commentMap["post_id"] = strconv.FormatInt(comment.PostID, 10)
	commentMap["user_id"] = strconv.FormatInt(comment.UserID, 10)
	message.Content = commentMap
	return message
}
