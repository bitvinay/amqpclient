package amqpclient

import "strconv"

//PostViewEventTransactionProcessor -
type PostViewEventTransactionProcessor struct {
}

//CreateEvent -
func (pevp PostViewEventTransactionProcessor) CreateEvent(amqpMessage AMQPMessage) PostViewEventTransaction {

	postMap := amqpMessage.Content.(map[string]interface{})
	postViewEvent := PostViewEventTransaction{}
	postViewEvent.UserID, _ = strconv.ParseInt(postMap["user_id"].(string), 10, 64)
	postViewEvent.AdvertisementID, _ = strconv.ParseInt(postMap["advt_id"].(string), 10, 64)
	postViewEvent.PostID, _ = strconv.ParseInt(postMap["post_id"].(string), 10, 64)
	postViewEvent.OwnerUserID, _ = strconv.ParseInt(postMap["owner_user_id"].(string), 10, 64)
	postViewEvent.ChannelID, _ = strconv.ParseInt(postMap["channel_id"].(string), 10, 64)
	return postViewEvent

}
