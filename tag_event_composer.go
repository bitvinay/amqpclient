package amqpclient

import (
	"os"
	"strconv"
)

//CreateEvent - create tag event
func (uec TagEventComposer) CreateEvent(tag TagAddModel) AMQPMessage {
	message := AMQPMessage{}
	message.Event = Event.Tag.CreateEvent
	tagMap := make(map[string]interface{})
	tagMap["id"] = strconv.FormatInt(tag.ID, 10)
	tagMap["name"] = tag.Name
	tagMap["created_by"] = strconv.FormatInt(tag.CreatedBy, 10)
	message.Sender = os.Getenv("app.name")
	message.Content = tagMap
	return message
}
