package amqpclient

import (
	"strconv"
	"time"
)

//CreateEvent -
func (uep SubscribeChannelEventProcessor) CreateEvent(amqpMessage AMQPMessage) SubscribeChannelAddModel {
	subscribeMap := amqpMessage.Content.(map[string]interface{})
	subscribeChannel := SubscribeChannelAddModel{}
	subscribeChannel.ID, _ = strconv.ParseInt(subscribeMap["id"].(string), 10, 64)
	subscribeChannel.ChannelID, _ = strconv.ParseInt(subscribeMap["channel_id"].(string), 10, 64)
	subscribeChannel.UserID, _ = strconv.ParseInt(subscribeMap["user_id"].(string), 10, 64)
	subscribeChannel.Active, _ = subscribeMap["active"].(bool)
	subscribeChannel.CreatedAt, _ = time.Parse(time.RFC3339, subscribeMap["created_at"].(string))
	return subscribeChannel
}

//UpdateEvent -
func (uep SubscribeChannelEventProcessor) UpdateEvent(amqpMessage AMQPMessage) SubscribeChannelUpdateModel {
	subscribeMap := amqpMessage.Content.(map[string]interface{})
	subscribeChannel := SubscribeChannelUpdateModel{}
	subscribeChannel.ID, _ = strconv.ParseInt(subscribeMap["id"].(string), 10, 64)
	subscribeChannel.Active, _ = subscribeMap["active"].(bool)
	subscribeChannel.UpdatedAt = subscribeMap["updated_at"].(time.Time)
	return subscribeChannel
}

//DeleteEvent -
func (uep SubscribeChannelEventProcessor) DeleteEvent(amqpMessage AMQPMessage) SubscribeChannelDeleteModel {
	subscribeChannel := SubscribeChannelDeleteModel{}
	subscribeMap := amqpMessage.Content.(map[string]interface{})
	subscribeChannel.ChannelID, _ = strconv.ParseInt(subscribeMap["channel_id"].(string), 10, 64)
	subscribeChannel.UserID, _ = strconv.ParseInt(subscribeMap["user_id"].(string), 10, 64)
	return subscribeChannel
}
