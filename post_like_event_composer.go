package amqpclient

import (
	"os"
	"strconv"
)

//CreateEvent -
func (pvec PostLikeEventComposer) CreateEvent(event PostLikeEvent) AMQPMessage {
	message := AMQPMessage{}
	message.Event = Event.Post.Like.CreateEvent
	postMap := make(map[string]interface{})

	postMap["id"] = strconv.FormatInt(event.ID, 10)
	postMap["user_id"] = strconv.FormatInt(event.UserID, 10)
	postMap["post_id"] = strconv.FormatInt(event.PostID, 10)

	message.Sender = os.Getenv("app.name")
	message.Content = postMap
	return message
}

//DeleteEvent -
func (pvec PostLikeEventComposer) DeleteEvent(event PostLikeEvent) AMQPMessage {
	message := AMQPMessage{}
	message.Event = Event.Post.Like.DeleteEvent
	postMap := make(map[string]interface{})

	postMap["user_id"] = strconv.FormatInt(event.UserID, 10)
	postMap["post_id"] = strconv.FormatInt(event.PostID, 10)

	message.Sender = os.Getenv("app.name")
	message.Content = postMap
	return message
}
