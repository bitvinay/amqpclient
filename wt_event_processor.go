package amqpclient

import "strconv"

type WalletTransactionProcessor struct {
}

//CreateEvent -
func (uep WalletTransactionProcessor) CreateEvent(amqpMessage AMQPMessage) WalletTransactionCreateModel {
	wtMap := amqpMessage.Content.(map[string]interface{})
	wt := WalletTransactionCreateModel{}
	wt.ID, _ = strconv.ParseInt(wtMap["id"].(string), 10, 64)

	if wtMap["comment"] != nil {
		wt.Comment = wtMap["comment"].(string)
	}
	if wtMap["external_transaction_id"] != nil {
		wt.ExternalTransactionID = wtMap["external_transaction_id"].(string)
	}
	if wtMap["from_source_id"] != nil {
		wt.ID, _ = strconv.ParseInt(wtMap["from_source_id"].(string), 10, 64)
	}
	if wtMap["status"] != nil {
		wt.Status, _ = strconv.Atoi(wtMap["status"].(string))
	}
	if wtMap["transaction_type"] != nil {
		wt.TransactionType, _ = strconv.Atoi(wtMap["transaction_type"].(string))
	}
	if wtMap["user_id"] != nil {
		wt.ID, _ = strconv.ParseInt(wtMap["user_id"].(string), 10, 64)
	}
	if wtMap["wallet_id"] != nil {
		wt.ID, _ = strconv.ParseInt(wtMap["wallet_id"].(string), 10, 64)
	}
	if wtMap["amount"] != nil {
		am, err := strconv.ParseFloat(wtMap["amount"].(string), 32)
		if err != nil {
			wt.Amount = float32(am)
		}
	}
	return wt
}

//UpdateEvent -
func (uep WalletTransactionProcessor) UpdateEvent(amqpMessage AMQPMessage) WalletTransactionUpdateModel {
	wtMap := amqpMessage.Content.(map[string]interface{})
	wt := WalletTransactionUpdateModel{}
	wt.ID, _ = strconv.ParseInt(wtMap["id"].(string), 10, 64)
	if wtMap["status"] != nil {
		wt.Status, _ = strconv.Atoi(wtMap["status"].(string))
	}
	return wt
}
