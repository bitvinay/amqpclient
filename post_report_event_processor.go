package amqpclient

import (
	"strconv"
)

//CreateEvent -
func (pevp PostReportEventProcessor) CreateEvent(amqpMessage AMQPMessage) PostReportEvent {

	postMap := amqpMessage.Content.(map[string]interface{})
	postReportEvent := PostReportEvent{}

	postReportEvent.UserID, _ = strconv.ParseInt(postMap["user_id"].(string), 10, 64)
	postReportEvent.PostID, _ = strconv.ParseInt(postMap["post_id"].(string), 10, 64)
	postReportEvent.ID, _ = strconv.ParseInt(postMap["id"].(string), 10, 64)
	postReportEvent.ReportType = postMap["report_type"].(string)
	if postMap["report_description"] != "" {
		postReportEvent.ReportDescription = postMap["report_description"].(string)
	}

	return postReportEvent

}
