package amqpclient

import (
	"strconv"
	"time"
)

//CreateEvent -
func (uep PostEventProcessor) CreateEvent(amqpMessage AMQPMessage) PostAddModel {
	postMap := amqpMessage.Content.(map[string]interface{})
	post := PostAddModel{}
	post.ID, _ = strconv.ParseInt(postMap["id"].(string), 10, 64)
	post.ChannelID, _ = strconv.ParseInt(postMap["channel_id"].(string), 10, 64)

	if postMap["text"] != nil {
		post.Text = postMap["text"].(string)
	}
	if postMap["attachments"] != nil {
		post.Attachments = postMap["attachments"].(string)
	}
	if postMap["tags"] != nil {
		post.Tags = postMap["tags"].(string)
	}
	post.UserID, _ = strconv.ParseInt(postMap["user_id"].(string), 10, 64)
	if postMap["created_at"] != nil {
		post.CreatedAt, _ = time.Parse(time.RFC3339, postMap["created_at"].(string))
	}
	return post
}

//DeleteEvent -
func (uep PostEventProcessor) DeleteEvent(amqpMessage AMQPMessage) PostDeleteModel {
	post := PostDeleteModel{}
	post.ID, _ = strconv.ParseInt(amqpMessage.Content.(string), 10, 64)
	return post
}
