package amqpclient

import "strconv"

//CreateEvent -
func (uep UserEventProcessor) CreateEvent(amqpMessage AMQPMessage) UserAddModel {
	userMap := amqpMessage.Content.(map[string]interface{})
	user := UserAddModel{}
	user.ID, _ = strconv.ParseInt(userMap["id"].(string), 10, 64)
	user.Active = userMap["active"].(bool)

	if userMap["user_name"] != nil {
		user.UserName = userMap["user_name"].(string)
	}
	if userMap["country"] != nil {
		user.Country = userMap["country"].(string)
	}
	if userMap["email"] != nil {
		user.Email = userMap["email"].(string)
	}
	if userMap["name"] != nil {
		user.Name = userMap["name"].(string)
	}
	if userMap["phone"] != nil {
		user.Phone = userMap["phone"].(string)
	}
	return user
}

//UpdateEvent -
func (uep UserEventProcessor) UpdateEvent(amqpMessage AMQPMessage) UserUpdateModel {
	userMap := amqpMessage.Content.(map[string]interface{})
	user := UserUpdateModel{}
	user.ID, _ = strconv.ParseInt(userMap["id"].(string), 10, 64)
	user.Active = userMap["active"].(bool)

	if userMap["user_name"] != nil {
		user.UserName = userMap["user_name"].(string)
	}
	if userMap["country"] != nil {
		user.Country = userMap["country"].(string)
	}
	if userMap["email"] != nil {
		user.Email = userMap["email"].(string)
	}
	if userMap["name"] != nil {
		user.Name = userMap["name"].(string)
	}
	if userMap["phone"] != nil {
		user.Phone = userMap["phone"].(string)
	}
	if userMap["gender"] != nil {
		user.Gender = userMap["gender"].(string)
	}
	if userMap["profile_pic_url"] != nil {
		user.ProfilePicURL = userMap["profile_pic_url"].(string)
	}
	if userMap["thumb_url"] != nil {
		user.ThumbPicURL = userMap["thumb_url"].(string)
	}
	if userMap["dob"] != nil {
		user.DOB = userMap["dob"].(string)
	}
	return user
}

//DeleteEvent -
func (uep UserEventProcessor) DeleteEvent(amqpMessage AMQPMessage) UserDeleteModel {
	user := UserDeleteModel{}
	user.ID, _ = strconv.ParseInt(amqpMessage.Content.(string), 10, 64)
	return user
}
