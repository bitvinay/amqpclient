package amqpclient

import "strconv"

//PostViewEventProcessor -
type PostViewEventProcessor struct {
}

//CreateEvent -
func (pevp PostViewEventProcessor) CreateEvent(amqpMessage AMQPMessage) PostViewEvent {

	postMap := amqpMessage.Content.(map[string]interface{})
	postViewEvent := PostViewEvent{}
	postViewEvent.UserID, _ = strconv.ParseInt(postMap["user_id"].(string), 10, 64)
	postViewEvent.AdvertisementID, _ = strconv.ParseInt(postMap["advt_id"].(string), 10, 64)
	postUser := make(map[string]int64)
	postIDSts := postMap["post_user"].(map[string]interface{})
	for postWithChannelID, userIDSt := range postIDSts {
		userID, _ := strconv.ParseInt(userIDSt.(string), 10, 64)
		postUser[postWithChannelID] = userID
	}
	postViewEvent.PostUser = postUser
	return postViewEvent

}
