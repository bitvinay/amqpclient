package amqpclient

import (
	"os"
	"strconv"
)

//CreateEvent -Channel event
func (cec ChannelEventComposer) CreateEvent(channel ChannelAddModel) AMQPMessage {
	message := AMQPMessage{}
	message.Event = Event.Channel.CreateEvent
	channelMap := make(map[string]interface{})
	channelMap["id"] = strconv.FormatInt(channel.ID, 10)
	channelMap["name"] = channel.Name
	channelMap["description"] = channel.Description
	channelMap["unique_name"] = channel.UniqueName
	channelMap["user_id"] = strconv.FormatInt(channel.UserID, 10)
	channelMap["type"] = channel.Type
	channelMap["created_at"] = channel.CreatedAt
	message.Content = channelMap
	message.Sender = os.Getenv("app.name")
	return message
}

//UpdateEvent -Channel event
func (cec ChannelEventComposer) UpdateEvent(channel ChannelUpdateModel) AMQPMessage {
	message := AMQPMessage{}
	message.Event = Event.Channel.UpdateEvent
	channelMap := make(map[string]interface{})
	channelMap["id"] = strconv.FormatInt(channel.ID, 10)
	channelMap["name"] = channel.Name
	channelMap["description"] = channel.Description
	channelMap["pic_url"] = channel.PicURL
	channelMap["thumb_url"] = channel.ThumbURL
	channelMap["type"] = channel.Type
	message.Content = channelMap
	message.Sender = os.Getenv("app.name")
	return message
}

//DeleteEvent -Channel event
func (cec ChannelEventComposer) DeleteEvent(channel ChannelDeleteModel) AMQPMessage {
	message := AMQPMessage{}
	message.Event = Event.Channel.DeleteEvent
	message.Sender = os.Getenv("app.name")
	message.Content = strconv.FormatInt(channel.ID, 10)
	return message
}
