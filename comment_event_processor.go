package amqpclient

import (
	"strconv"
	"time"
)

//CreateEvent -
func (uep CommentEventProcessor) CreateEvent(amqpMessage AMQPMessage) CommentAddModel {
	commentMap := amqpMessage.Content.(map[string]interface{})
	comment := CommentAddModel{}
	comment.ID, _ = strconv.ParseInt(commentMap["id"].(string), 10, 64)
	comment.PostID, _ = strconv.ParseInt(commentMap["post_id"].(string), 10, 64)
	comment.ParentID, _ = strconv.ParseInt(commentMap["parent_id"].(string), 10, 64)
	comment.UserID, _ = strconv.ParseInt(commentMap["user_id"].(string), 10, 64)
	comment.Text = commentMap["text"].(string)
	if commentMap["created_at"] != nil {
		comment.CreatedAt, _ = time.Parse(time.RFC3339, commentMap["created_at"].(string))
	}
	return comment
}

//UpdateEvent -
func (uep CommentEventProcessor) UpdateEvent(amqpMessage AMQPMessage) CommentUpdateModel {
	commentMap := amqpMessage.Content.(map[string]interface{})
	comment := CommentUpdateModel{}
	comment.ID, _ = strconv.ParseInt(commentMap["id"].(string), 10, 64)
	comment.Text = commentMap["text"].(string)
	if commentMap["updated_at"] != nil {
		comment.UpdatedAt, _ = time.Parse(time.RFC3339, commentMap["updated_at"].(string))
	}
	return comment
}

//DeleteEvent -
func (uep CommentEventProcessor) DeleteEvent(amqpMessage AMQPMessage) CommentDeleteModel {
	comment := CommentDeleteModel{}
	commentMap := amqpMessage.Content.(map[string]interface{})
	comment.ID, _ = strconv.ParseInt(commentMap["id"].(string), 10, 64)
	comment.PostID, _ = strconv.ParseInt(commentMap["post_id"].(string), 10, 64)
	comment.UserID, _ = strconv.ParseInt(commentMap["user_id"].(string), 10, 64)
	return comment
}
