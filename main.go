package amqpclient

import (
	log "github.com/sirupsen/logrus"
)

//SampleReceiver -
func SampleReceiver() {
	forever := make(chan bool)
	amqpClient := GetInstance()
	amqpClient.New("host", "username", "password")
	amqpMessage := AMQPMessage{}
	amqpMessage.Content = "Testing"
	amqpMessage.Event = "sample"
	amqpMessage.Sender = "Test sender"
	amqpMessage.ToExchange = "test_exchange"

	connected := <-amqpClient.Connected
	if connected {
		_, err := amqpClient.AMQPSession.Listen("test_queue")
		if err != nil {
			log.Errorln(err)
		}
	}

	<-forever
}

//SampleSender  -
func SampleSender() {
	amqpClient := GetInstance()

	amqpClient.New("host", "username", "password")

	amqpMessage := AMQPMessage{}
	amqpMessage.Content = "Testing"
	amqpMessage.Event = "sample"
	amqpMessage.Sender = "Test sender"
	amqpMessage.ToExchange = "test_exchange"
	connected := <-amqpClient.Connected

	if connected {
		err := amqpClient.AMQPSession.Send("test_exchange", amqpMessage)
		if err != nil {
			log.Errorln(err)
		}
	}
}
