package amqpclient

import (
	"strconv"
	"time"
)

//CreateEvent -
func (pevp ApprovalEventProcessor) CreateEvent(amqpMessage AMQPMessage) ApprovalEvent {

	approvalMap := amqpMessage.Content.(map[string]interface{})
	approvalEvent := ApprovalEvent{}

	approvalEvent.UserID, _ = strconv.ParseInt(approvalMap["user_id"].(string), 10, 64)
	approvalEvent.RequestedBy, _ = strconv.ParseInt(approvalMap["requested_by"].(string), 10, 64)
	approvalEvent.ResourceID, _ = strconv.ParseInt(approvalMap["resource_id"].(string), 10, 64)
	approvalEvent.ID, _ = strconv.ParseInt(approvalMap["id"].(string), 10, 64)
	approvalEvent.ResourceType = approvalMap["resource_type"].(string)
	approvalEvent.Status = approvalMap["status"].(bool)

	if approvalMap["comment"] != "" {
		approvalEvent.Comment = approvalMap["comment"].(string)
	}

	approvalEvent.CreatedAt, _ = time.Parse(time.RFC3339, approvalMap["created_at"].(string))
	return approvalEvent

}
