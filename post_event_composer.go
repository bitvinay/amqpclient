package amqpclient

import (
	"os"
	"strconv"
)

//CreateEvent - create user event
func (uec PostEventComposer) CreateEvent(post PostAddModel) AMQPMessage {
	message := AMQPMessage{}
	message.Event = Event.Post.CreateEvent
	userMap := make(map[string]interface{})
	userMap["id"] = strconv.FormatInt(post.ID, 10)
	userMap["channel_id"] = strconv.FormatInt(post.ChannelID, 10)
	userMap["text"] = post.Text
	userMap["attachments"] = post.Attachments
	userMap["tags"] = post.Tags
	userMap["user_id"] = strconv.FormatInt(post.UserID, 10)
	userMap["created_at"] = post.CreatedAt
	message.Sender = os.Getenv("app.name")
	message.Content = userMap
	return message
}

//DeleteEvent -
func (uec PostEventComposer) DeleteEvent(post PostDeleteModel) AMQPMessage {
	message := AMQPMessage{}
	message.Event = Event.Post.DeleteEvent
	message.Sender = os.Getenv("app.name")
	message.Content = strconv.FormatInt(post.ID, 10)
	return message
}
