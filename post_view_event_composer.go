package amqpclient

import (
	"os"
	"strconv"
)

//PostViewEventComposer -
type PostViewEventComposer struct {
}

//CreateEvent -
func (pvec PostViewEventComposer) CreateEvent(event PostViewEvent) AMQPMessage {
	message := AMQPMessage{}
	message.Event = Event.Post.View
	postMap := make(map[string]interface{})
	postUserMap := make(map[string]string)

	for postWithChannelID, userID := range event.PostUser {
		k := postWithChannelID
		v := strconv.FormatInt(userID, 10)
		postUserMap[k] = v

	}
	postMap["post_user"] = postUserMap
	postMap["user_id"] = strconv.FormatInt(event.UserID, 10)
	postMap["advt_id"] = strconv.FormatInt(event.AdvertisementID, 10)

	message.Sender = os.Getenv("app.name")
	message.Content = postMap
	return message
}
